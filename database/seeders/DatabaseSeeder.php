<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();
        // \App\Models\Course::factory(5000)
        //     ->has(\App\Models\Participant::factory(5))
        //     ->create();
        // \App\Models\Journal::factory(1000000)->create();
        \App\Models\Price::factory()->create();
        // \App\Models\Sale::factory()
        //     ->count(10000)
        //     ->create();
    }
}
