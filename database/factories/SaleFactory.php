<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Article;
use App\Models\Price;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Course>
 */
class SaleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $price = Price::find(1);
        if(is_null($price)) {
            $price = Price::factory()->create();
        }
        return [
            'SAL_ART_NO' => $price->PRC_ART_NO,
            'SAL_PRC_NO' => $price->PRC_NO,
            'SAL_TYPE' => $this->faker->numberBetween(0, 10),
            'SAL_DATE_FROM' => $this->faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null),
            'SAL_DATE_TO' => $this->faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null),
            'SAL_ADDITIONAL_PROPERTIES' => '[{"text": {"cs": "<p>CS Leistung 1</p>", "de": "<ul><li><p>Leistung 1</p></li><li><p>Leistung 2</p></li></ul>"}, "headline": {"cs": "Leistungen CS", "da": "Leistungen DA", "de": "Leistungen"}}, {"text": {"de": "<p>Nullam cursus lacinia erat. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Vestibulum fringilla pede sit amet augue.</p>"}, "headline": {"de": "Leistungen 2"}}]',
        ];
    }
}
