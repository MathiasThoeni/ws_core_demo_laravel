<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Article;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Course>
 */
class PriceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'PRC_ART_NO' => Article::factory()->create()->ART_NO,
            'PRC_DATE_FROM' => $this->faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null),
            'PRC_DATE_TO' => $this->faker->dateTimeBetween($startDate = '-4 years', $endDate = 'now', $timezone = null),
            'PRC_PRICE' => $this->faker->numberBetween(0, 1000),
            'PRC_PERCENTAGE' => $this->faker->numberBetween(0, 100),
        ];
    }
}
