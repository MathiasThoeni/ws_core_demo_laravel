<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Course>
 */
class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'ART_NAME' => $this->faker->name(),
            'ART_TYPE' => $this->faker->numberBetween(1, 100),
            'ART_INFORMATION' => '[{"text": {"cs": "<p>CS Leistung 1</p>", "de": "<ul><li><p>Leistung 1</p></li><li><p>Leistung 2</p></li></ul>"}, "headline": {"cs": "Leistungen CS", "da": "Leistungen DA", "de": "Leistungen"}}, {"text": {"de": "<p>Nullam cursus lacinia erat. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Vestibulum fringilla pede sit amet augue.</p>"}, "headline": {"de": "Leistungen 2"}}]',
            'ART_ACTIVE' => $this->faker->boolean(),
        ];
    }
}
