<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('td_article', function (Blueprint $table) {
            $table->id('ART_NO')->nullable(false);
            $table->string('ART_NAME')->nullable(false);
            $table->smallInteger('ART_TYPE')->nullable(false);
            $table->json('ART_INFORMATION');
            $table->boolean('ART_ACTIVE')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('td_article');
    }
};
