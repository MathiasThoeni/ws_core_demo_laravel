<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tf_sale', function (Blueprint $table) {
            $table->id('SAL_NO')->nullable(false);
            $table->foreignId('SAL_ART_NO')->references('ART_NO')->on('td_article')->nullable(false);
            $table->foreignId('SAL_PRC_NO')->references('PRC_NO')->on('td_price')->nullable(false);
            $table->smallInteger('SAL_TYPE')->nullable(false);
            $table->date('SAL_DATE_FROM')->nullable(false);
            $table->date('SAL_DATE_TO')->nullable(false);
            $table->json('SAL_ADDITIONAL_PROPERTIES');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tf_sale');
    }
};
