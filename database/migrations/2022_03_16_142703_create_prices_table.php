<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('td_price', function (Blueprint $table) {
            $table->id('PRC_NO')->nullable(false);
            $table->foreignId('PRC_ART_NO')->references('ART_NO')->on('td_article')->nullable(false);
            $table->date('PRC_DATE_FROM')->nullable(false);
            $table->date('PRC_DATE_TO')->nullable(false);
            $table->smallInteger('PRC_PRICE')->nullable(false);
            $table->smallInteger('PRC_PERCENTAGE')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('td_price');
    }
};
