<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tf_event', function (Blueprint $table) {
            $table->id('EVN_NO')->nullable(false);
            $table->foreignId('EVN_ART_NO')->references('ART_NO')->on('td_article')->nullable(false);
            $table->date('EVN_DATE_STARTDAY')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tf_event');
    }
};
