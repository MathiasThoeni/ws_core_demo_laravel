drop table if exists tf_sale;
drop table if exists tf_event;
drop table if exists tf_sale;
drop table if exists td_price;
drop table if exists td_article;

-- achtung indizes noch keine angelegt

-- 1000 artikel
-- ART_INFORMATION ein beispiel: [{"text": {"cs": "<p>CS Leistung 1</p>", "de": "<ul><li><p>Leistung 1</p></li><li><p>Leistung 2</p></li></ul>"}, "headline": {"cs": "Leistungen CS", "da": "Leistungen DA", "de": "Leistungen"}}, {"text": {"de": "<p>Nullam cursus lacinia erat. Nullam nulla eros, ultricies sit amet, nonummy id, imperdiet feugiat, pede. Vestibulum fringilla pede sit amet augue.</p>"}, "headline": {"de": "Leistungen 2"}}]
CREATE TABLE td_article (
	ART_NO serial not null,
	ART_NAME VARCHAR(100) NOT NULL,
	ART_TYPE SMALLINT NOT NULL, -- 1 Gruppenkurs, ...
	ART_INFORMATION JSONB,
	ART_ACTIVE SMALLINT DEFAULT 0 NOT NULL
) ;

-- für jeden artikel einen preis 01.01.xxx - 31.12.xxx -> ab 2019 - 2022
CREATE TABLE td_price (
	PRC_NO serial not null,
	PRC_ART_NO BIGINT NOT NULL,
	PRC_DATE_FROM DATE not null,
	PRC_DATE_TO DATE not null,
	PRC_PRICE SMALLINT NOT NULL,
	PRC_PERCENTAGE SMALLINT NOT NULL
) ;

-- für jeden artikel einen starttag von 01.01.xxx - 31.12.xxx -> ab 2019 - 2022
-- nur zu testzwecken wie schnell insert geht und abfrage - würde article_scheduledevent entsprechen
-- ob das in Zukunft weiterhin so sein wird werden wir sehen
CREATE TABLE tf_event (
	EVN_NO serial not null,
	EVN_ART_NO BIGINT NOT NULL,
	EVN_DATE_STARTDAY DATE not null
) ;

-- ca 10000 Verkäufe
CREATE TABLE tf_sale (
	SAL_NO serial not null,
	SAL_ART_NO BIGINT NOT NULL,
	SAL_PRC_NO BIGINT NOT NULL,
	SAL_TYPE smallint not null, -- 1 reservation, 2 sale
	SAL_DATE_FROM DATE not null,
	SAL_DATE_TO DATE not null,
	SAL_ADDITONAL_PROPERTIES JSONB
) ;

ALTER TABLE "public".td_article ADD CONSTRAINT pky_td_article_00 PRIMARY KEY (art_no) ;

ALTER TABLE "public".td_price ADD CONSTRAINT pky_td_price_00 PRIMARY KEY (prc_no) ;
ALTER TABLE "public".td_price ADD CONSTRAINT fky_prcart_00 FOREIGN KEY (prc_art_no) REFERENCES "public".td_article(art_no) ;

ALTER TABLE "public".tf_sale ADD CONSTRAINT pky_tf_sale_00 PRIMARY KEY (sal_no) ;
ALTER TABLE "public".tf_sale ADD CONSTRAINT fky_salart_00 FOREIGN KEY (sal_art_no) REFERENCES "public".td_article(art_no) ;
ALTER TABLE "public".tf_sale ADD CONSTRAINT fky_salprc_01 FOREIGN KEY (sal_prc_no) REFERENCES "public".td_price(prc_no) ;

ALTER TABLE "public".tf_event ADD CONSTRAINT pky_tf_event_00 PRIMARY KEY (evn_no) ;
ALTER TABLE "public".tf_event ADD CONSTRAINT fky_evnart_00 FOREIGN KEY (evn_art_no) REFERENCES "public".td_article(art_no) ;
