@extends('layouts.app')

@section('title', 'Events')

@section('content')
<div class="relative flex items-top justify-center min-h-screen bg-gray-100 sm:items-center py-4 sm:pt-0">
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-transparent pt-8 sm:justify-start sm:pt-0" style="text-align: center; padding-top: 1em; padding-bottom: 1em;">
            <img class="mx-auto" src="https://www.waldhart.at/fileadmin/user_upload/logo/logo_white.svg"/>
        </div>

        <div id="app">
            <calendar-component></calendar-component>
        </div>
    </div>
</div>
@endsection
