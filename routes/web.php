<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\JournalController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\EventController;
use App\Http\Resources\CourseResource;
use App\Models\Course;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/courses', function () {
    $time_start = microtime(true);

    $courses = Course::join('participants', 'participants.course_id', '=', 'courses.id')->get();

    $time_end = microtime(true);
    $execution_time = ($time_end - $time_start);
    echo '<b>Total Execution Time:</b> '.($execution_time*1000).' Milliseconds <br>';

    $time_start = microtime(true);

    $ret_val = CourseResource::collection($courses);

    $time_end = microtime(true);
    $execution_time = ($time_end - $time_start);
    echo '<b>Total Collection Time:</b> '.($execution_time*1000).' Milliseconds';
});

Route::get('/courses/{course}', [CourseController::class, 'show']);

Route::get('/journals', [JournalController::class, 'index']);

Route::get('/sales', [SaleController::class, 'index']);

Route::get('/events', [EventController::class, 'index']);
