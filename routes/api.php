<?php

use App\Http\Controllers\Api\ArticleApiController;
use App\Http\Controllers\JournalController;
use App\Http\Controllers\BenchmarkController;
use App\Http\Controllers\Api\SaleApiController;
use App\Http\Controllers\Api\EventApiController;
use App\Http\Controllers\Api\PriceApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/journals', [JournalController::class, 'journalData']);

Route::resource('/articles', ArticleApiController::class);

Route::resource('/sales', SaleApiController::class);

Route::resource('/prices', PriceApiController::class);

Route::resource('/events', EventApiController::class);

Route::get('/benchmark/create-articles', [BenchmarkController::class, 'createArticles']);

Route::get('/benchmark/create-prices', [BenchmarkController::class, 'createPrices']);

Route::get('/benchmark/create-sales', [BenchmarkController::class, 'createSales']);

Route::get('/benchmark/retrieve-sales', [BenchmarkController::class, 'retrieveSales']);
