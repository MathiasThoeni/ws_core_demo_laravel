<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    use HasFactory;

    protected $table = 'td_price';
    protected $primaryKey = 'PRC_NO';

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'PRC_ART_NO',
        'PRC_PRICE',
        'PRC_PERCENTAGE',
        'PRC_DATE_FROM',
        'PRC_DATE_TO',
    ];
}
