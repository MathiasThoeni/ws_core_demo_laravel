<?php

namespace App\Models;

use App\Events\SaleAdded;
use App\Events\SaleDeleted;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;

    protected $table = 'tf_sale';
    protected $primaryKey = 'SAL_NO';

    protected $casts = [
        'SAL_ADDITIONAL_PROPERTIES' => 'array'
    ];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function price()
    {
        return $this->belongsTo(Price::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'SAL_ART_NO',
        'SAL_PRC_NO',
        'SAL_TYPE',
        'SAL_DATE_FROM',
        'SAL_DATE_TO',
        'SAL_ADDITIONAL_PROPERTIES',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => SaleAdded::class,
        'deleted' => SaleDeleted::class,
    ];
}
