<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $table = 'td_article';
    protected $primaryKey = 'ART_NO';

    protected $casts = [
        'ART_INFORMATION' => 'array'
    ];

    public function sales(){
        return $this->hasMany(Sale::class);
    }
}
