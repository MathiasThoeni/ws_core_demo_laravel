<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Events\EventAdded;
use App\Events\EventDeleted;

class Event extends Model
{
    use HasFactory;

    protected $table = 'tf_event';
    protected $primaryKey = 'EVN_NO';

    public function article()
    {
        return $this->belongsTo(Article::class, 'EVN_ART_NO');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'EVN_ART_NO',
        'EVN_DATE_STARTDAY',
    ];

    protected $dispatchesEvents = [
        'created' => EventAdded::class,
        'deleted' => EventDeleted::class,
    ];
}
