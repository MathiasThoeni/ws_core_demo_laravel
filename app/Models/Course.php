<?php

namespace App\Models;

use App\Events\CourseInfoUpdated;
use App\Events\CourseDeleted;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Course extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'date',
        'meeting_point',
        'meeting_time',
    ];

    public function participants()
    {
        return $this->hasMany(Participant::class);
    }

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'updated' => CourseInfoUpdated::class,
        'deleted' => CourseDeleted::class,
    ];
}
