<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Journal;
use App\Http\Resources\JournalResource;

class JournalController extends Controller
{

    public function index()
    {
        $journals = Journal::paginate(10);
        return view('journals.index', compact('journals'));
    }

    public function show(Journal $journal)
    {
        return view('journals.show', compact('journal'));
    }

    public function journalData()
    {
        $journals = Journal::paginate(15);
        return JournalResource::collection($journals);
    }
}
