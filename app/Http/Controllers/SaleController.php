<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sale;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sales.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->missing([
            'artType',
            'SAL_PRC_NO',
            'SAL_TYPE',
            'SAL_DATE_FROM',
            'SAL_DATE_TO',
            'SAL_ADDITIONAL_PROPERTIES'
        ])) {
            return False;
        }

        $sale = new Sale;
        $sale->SAL_ART_NO = $request->artType;
        $sale->SAL_PRC_NO = $request->SAL_PRC_NO;
        $sale->SAL_TYPE = $request->SAL_TYPE;
        $sale->SAL_DATE_FROM = $request->SAL_DATE_FROM;
        $sale->SAL_DATE_TO = $request->SAL_DATE_TO;
        $sale->SAL_ADDITIONAL_PROPERTIES = $request->SAL_ADDITIONAL_PROPERTIES;
        $sale->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function calendar()
    {
        return view('sales.calendar');
    }
}
