<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Sale;
use App\Http\Resources\SaleResource;

class SaleApiController extends ApiController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return SaleResource::collection(
            Sale::join('td_article', 'tf_sale.SAL_ART_NO', '=', 'td_article.ART_NO')
                ->join('td_price', 'tf_sale.SAL_PRC_NO', '=', 'td_price.PRC_NO')
                ->orderBy('SAL_DATE_FROM', 'desc')->get()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'salType' => 'required|integer',
            'salDateFrom' => 'required|date',
            'salDateTo' => 'required|date',
            'salAdditionalProperties' => 'array',
            'salArticle' => 'required|array',
            'salPrice' => 'required|array',
        ]);

        $sale = new Sale();
        $sale->SAL_ART_NO = $request->salArticle['artNo'];
        $sale->SAL_PRC_NO = $request->salPrice['prcNo'];
        $sale->SAL_TYPE = $request->salType;
        $sale->SAL_DATE_FROM = $request->salDateFrom;
        $sale->SAL_DATE_TO = $request->salDateTo;
        $sale->SAL_ADDITIONAL_PROPERTIES = $request->salAdditionalProperties;
        $sale->save();

        return new SaleResource($sale);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new SaleResource(Sale::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'salType' => 'integer',
            'salDateFrom' => 'date',
            'salDateTo' => 'date',
            'salAdditionalProperties' => 'array',
            'salArticle' => 'array',
            'salPrice' => 'array',
        ]);

        $sale = Sale::findOrFail($id);
        if(!is_null($request->salType)){
            $sale->SAL_TYPE = $request->salType;
        }
        if(!is_null($request->salDateFrom)){
            $sale->SAL_DATE_FROM = $request->salDateFrom;
        }
        if(!is_null($request->salDateTo)){
            $sale->SAL_DATE_TO = $request->salDateTo;
        }
        if(!is_null($request->salAdditionalProperties)){
            $sale->SAL_ADDITIONAL_PROPERTIES = $request->salAdditionalProperties;
        }
        if(!is_null($request->salArticle)){
            $sale->SAL_ART_NO = $request->salArticle['artNo'];
        }
        if(!is_null($request->salPrice)){
            $sale->SAL_PRC_NO = $request->salPrice['prcNo'];
        }
        $sale->save();

        return new SaleResource($sale);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sale = Sale::findOrFail($id);
        $sale->delete();

        return true;
    }

}
