<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Http\Resources\EventResource;

class EventApiController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $year = $request->input('year');
        $month = $request->input('month');
        if(is_null($year) && is_null($month)){
            return EventResource::collection(
                Event::all()
            );
        }
        return $this->getEvents($year, $month);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'evnDateStartday' => 'required|date',
            'evnArticle' => 'required|array',
        ]);

        $event = new Event();
        $event->EVN_ART_NO = $request->evnArticle['artNo'];
        $event->EVN_DATE_STARTDAY = $request->evnDateStartday;
        $event->save();

        return new EventResource($event);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new EventResource(Event::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'evnDateStartday' => 'date',
            'evnArticle' => 'array',
        ]);

        $event = Event::findOrFail($id);
        if(!is_null($request->evnDateStartday)){
            $event->EVN_DATE_STARTDAY = $request->evnDateStartday;
        }
        if(!is_null($request->evnArticle)){
            $event->EVN_ART_NO = $request->evnArticle['artNo'];
        }
        $event->save();

        return new EventResource($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::findOrFail($id);
        $event->delete();

        return true;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function get
    public function getEvents($year, $month)
    {
        if(is_null($year)) {
            $year = date('Y');
        }
        if(is_null($month)) {
            $month = date('m');
        }
        return EventResource::collection(
            Event::join('td_article', 'tf_event.EVN_ART_NO', '=', 'td_article.ART_NO')
                ->whereYear('EVN_DATE_STARTDAY', $year)
                ->whereMonth('EVN_DATE_STARTDAY', $month)
                ->get()
        );
    }
}
