<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Http\Resources\ArticleResource;

class ArticleApiController extends ApiController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ArticleResource::collection(
            Article::all()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'artName' => 'required|string|max:255',
            'artType' => 'required|integer',
            'artActive' => 'required|integer',
            'artInformation' => 'required|array',
        ]);

        $article = new Article();
        $article->ART_TYPE = $request->artType;
        $article->ART_NAME = $request->artName;
        $article->ART_ACTIVE = $request->artActive;
        $article->ART_INFORMATION = $request->artInformation;
        $article->save();

        return new ArticleResource($article);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ArticleResource(Article::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'artName' => 'string|max:255',
            'artType' => 'integer',
            'artActive' => 'integer',
            'artInformation' => 'array',
        ]);

        $article = Article::findOrFail($id);
        if(!is_null($request->artType)){
            $article->ART_TYPE = $request->artType;
        }
        if(!is_null($request->artName)){
            $article->ART_NAME = $request->artName;
        }
        if(!is_null($request->artActive)){
            $article->ART_ACTIVE = $request->artActive;
        }
        if(!is_null($request->artInformation)){
            $article->ART_INFORMATION = $request->artInformation;
        }
        $article->save();

        return new ArticleResource($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();

        return true;
    }

}
