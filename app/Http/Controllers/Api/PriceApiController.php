<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Price;
use App\Http\Resources\PriceResource;

class PriceApiController extends ApiController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PriceResource::collection(
            Price::all()
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'prcDateFrom' => 'required|date',
            'prcDateTo' => 'required|date',
            'prcPrice' => 'required|numeric',
            'prcPercentage' => 'required|integer',
            'prcArticle' => 'required|array',
        ]);

        $price = new Price();
        $price->PRC_DATE_FROM = $request->prcDateFrom;
        $price->PRC_DATE_TO = $request->prcDateTo;
        $price->PRC_PRICE = $request->prcPrice;
        $price->PRC_PERCENTAGE = $request->prcPercentage;
        $price->PRC_ART_NO = $request->prcArticle['artNo'];
        $price->save();

        return new PriceResource($price);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new PriceResource(Price::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'prcDateFrom' => 'date',
            'prcDateTo' => 'date',
            'prcPrice' => 'numeric',
            'prcPercentage' => 'integer',
            'prcArticle' => 'array',
        ]);

        $price = Price::findOrFail($id);
        if(!is_null($request->prcDateFrom)){
            $price->PRC_DATE_FROM = $request->prcDateFrom;
        }
        if(!is_null($request->prcDateTo)){
            $price->PRC_DATE_TO = $request->prcDateTo;
        }
        if(!is_null($request->prcPrice)){
            $price->PRC_PRICE = $request->prcPrice;
        }
        if(!is_null($request->prcPercentage)){
            $price->PRC_PERCENTAGE = $request->prcPercentage;
        }
        if(!is_null($request->prcArticle)){
            $price->PRC_ART_NO = $request->prcArticle['artNo'];
        }
        $price->save();

        return new PriceResource($price);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $price = Price::findOrFail($id);
        $price->delete();

        return true;
    }

}
