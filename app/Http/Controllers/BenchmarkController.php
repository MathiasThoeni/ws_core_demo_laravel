<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Price;
use App\Models\Sale;
use App\Models\Article;

class BenchmarkController extends Controller
{
    public function createArticles()
    {
        $sales = Article::factory()->count(100);
        $time_start = microtime(true);

        $sales->create();

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        return response()->json([
            'execution_time_milliseconds' => $execution_time*1000,
        ]);
    }

    public function createPrices()
    {
        $prices = Price::factory()->count(100);
        $time_start = microtime(true);

        $prices->create();

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        return response()->json([
            'execution_time_milliseconds' => $execution_time*1000,
        ]);
    }

    public function createSales()
    {
        $time_start = microtime(true);
        $sales = Sale::factory()->count(50000)->make();

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);

        $time_start = microtime(true);

        foreach($sales as $sale) {
            $sale->save();
        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        return response()->json([
            'execution_time_milliseconds' => $execution_time*1000,
        ]);
    }

    public function retrieveSales()
    {
        $time_start = microtime(true);

        $sales = Sale::all();

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        return response()->json([
            'execution_time_milliseconds' => $execution_time*1000,
        ]);
    }
}
