<p align="center"><a href="https://waldhart.at" target="_blank"><img src="https://demo.skischool.shop/fileadmin/user_upload/logo/logo.png" width="400"></a></p>

# Prerequisites

 - PHP
 - Composer
 - SQLite driver (not needed when using Docker/Sail)


# Database

In order to use WS Core, you need to setup a database connection. The `.env` file already includes three configurations for MySQL, PostgreSQL and SQLite. The default database is SQLite. If you want to use the SQLite connection you simply need to create a `database.sqlite` file in the `/database` folder.


# Installation

```bash
composer install
```

```bash
php artisan key:generate
```

## With Docker/Sail

Make sure you have `docker-compose` installed and the docker service is running. Inside the project root folder execute following commands:

```bash
./vendor/bin/sail up
```
This command will start all services defined in the `docker-compose.yaml` file and automatically serve the application on `localhost:8080`.

To create the needed database tables run:

```bash
./vendor/bin/sail php artisan migrate
```


## Without Docker/Sail

Migrate the database tables by executing following command in the application root folder:

```bash
php artisan migrate
```

If a driver error is thrown, check if SQLite is installed and this line is uncommented in your `php.ini` (usually in `/etc/php`):

```
extension=pdo_sqlite
```

Start the application:

```bash
php artisan serve
```
